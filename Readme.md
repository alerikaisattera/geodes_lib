# Geodes Library

Register a geode:
```lua
geodes_lib:register_geode({
    wherein = -- node in which geodes generate, set to mapgen_stone unless you need it to spawn in a specific place,
    y_min = -- minimum level at which geodes generate,
    y_max = -- maximum level at which geodes generate,
    scarcity = -- rarity of geodes, rarest at 80, lower values not recommended unless you use generation chance,
    core = -- node comprising innermost part of geode, usually a crystal such as amethyst,
    core_alt = -- replacement for node comprising innermost part of geode, usually a budding node which spawns plantlike drawtype crystal nodes on all sides,
    core_alt_chance = -- chance of core_alt replacing core,
    shell = -- array of nodes comprising geode shell, from outermost to innermost,
    radius_min = -- minimum radius of geode cavity,
    radius_max = -- maximum radius of geode cavity,
	generation_chance = -- % chance a geode spawns given other conditions, lets geodes be even rarer,
})
```
Example:
```lua
geodes_lib:register_geode({
    wherein = "mapgen_stone",
    y_min = -31000,
    y_max = -10,
    scarcity = 80,
    core = "modname:citrine",
    core_alt = "modname:citrine_budding",
    core_alt_chance = 100,
    shell = {"modname:limestone", "modname:calcite"},
    radius_min = 2,
    radius_max = 10,
	generation_chance = 50, -- set to around 10 if you are adding 20 or more geodes this way
})
```
